package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Valid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testIsValidLoginSpecialRegular( ) {
		assertTrue("Valid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testIsValidLoginSpecialException( ) {
		assertFalse("Invalid Login. No Special Characters and "
				+ "cannot start with numbers" , LoginValidator.isValidLoginName( "ra\\ses" ) );
	}
	
	@Test
	public void testIsValidLoginSpecialBoundaryIn( ) {
		assertTrue("Valid login" , LoginValidator.isValidLoginName( "r1mses" ) );
	}
	
	@Test
	public void testIsValidLoginSpecialBoundaryOut( ) {
		assertFalse("Invalid Login. No Special Characters and "
				+ "cannot start with numbers" , LoginValidator.isValidLoginName( "1amses" ) );
	}
	
	@Test
	public void testIsValidLoginLengthRegular( ) {
		assertTrue("Valid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testIsValidLoginLengthException( ) {
		assertFalse("Invalid Login. Must be at "
				+ "least 6 characters long" , LoginValidator.isValidLoginName( "abc" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryIn( ) {
		assertTrue("Valid Login" , LoginValidator.isValidLoginName( "abcdef" ) );
	}
	
	@Test
	public void testIsValidLoginLengthBoundaryOut( ) {
		assertFalse("Invalid Login. Must be at "
				+ "least 6 characters long" , LoginValidator.isValidLoginName( "abcde" ) );
	}
	
	

}
