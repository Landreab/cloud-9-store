package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		int specialCounter = 0;
		int startNumber = 0;
		
		if("abcdefghjiklmnopqrstuvwxyz".indexOf(loginName.charAt(0)) == -1) {
			startNumber++;
		}
		
		for (int i = 0; i < loginName.length(); i++) {
			if ("abcdefghijklmnopqrstuvqxyz1234567890".indexOf(loginName.charAt(i)) == -1) {
				specialCounter++;
			}
		}
		
		if(startNumber!=0) {
			return false;
		}
		
		if(specialCounter!=0) {
			return false;
		}
		else if (loginName.length() < 6) {
			return false;
		}
		
		else {
			return true;
		}
	}
}
